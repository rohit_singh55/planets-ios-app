//
//  OterSpaceTableTableViewController.m
//  Out of These World
//
//  Created by Rohit Singh on 04/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "OterSpaceTableTableViewController.h"
#import "AstronomicalData.h"
#import "OTWObject.h"
#import "ImageViewController.h"

#import "SpaceDataViewController.h"

@interface OterSpaceTableTableViewController ()

@end

@implementation OterSpaceTableTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.planets=[[NSMutableArray alloc]init];
    
    UIColor *backgroundColor = [UIColor blackColor];
    self.tableView.backgroundView = [[UIView alloc]initWithFrame:self.tableView.bounds];
    self.tableView.backgroundView.backgroundColor = backgroundColor;
    for (NSMutableDictionary *planeData in [AstronomicalData allKnownPlanets]) {
        NSString *imageName=[NSString stringWithFormat:@"%@.jpg",planeData[PLANET_NAME]];
        OTWObject *planet=[[OTWObject alloc]initWithData:planeData andImage:[UIImage imageNamed:imageName]];
        [self.planets addObject:planet];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        if([segue.destinationViewController isKindOfClass:[ImageViewController class]]){
            
            ImageViewController *imviewcontroller=segue.destinationViewController;
            NSIndexPath *path=[self.tableView indexPathForCell:sender];
            OTWObject *oobject=[self.planets objectAtIndex:path.row];
            imviewcontroller.otobject=oobject;
            
        }
    }
    if([sender isKindOfClass:[NSIndexPath class]]){
        if([segue.destinationViewController isKindOfClass:[SpaceDataViewController class]]){
            
            SpaceDataViewController *spviewcontroller=segue.destinationViewController;
            NSIndexPath *path=[self.tableView indexPathForCell:sender];
            OTWObject *oobject=[self.planets objectAtIndex:path.row];
           spviewcontroller.oobject=oobject;
        }
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
  
    return [self.planets count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuse=@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuse forIndexPath:indexPath];
    OTWObject *planet=[self.planets objectAtIndex:indexPath.row];
    cell.textLabel.text=planet.name;
    cell.detailTextLabel.text=planet.nickName;
    cell.imageView.image=planet.spaceImage;
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.detailTextLabel.textColor=[UIColor colorWithWhite:0.5 alpha:1.0];
  
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{

    [self performSegueWithIdentifier:@"push to space data" sender:indexPath];
}



@end
