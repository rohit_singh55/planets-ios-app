//
//  ImageViewController.h
//  Out of These World
//
//  Created by Rohit Singh on 04/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OTWObject.h"
@interface ImageViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property(strong,nonatomic) OTWObject *otobject;

@end
