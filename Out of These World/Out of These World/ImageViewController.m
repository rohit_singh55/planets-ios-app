//
//  ImageViewController.m
//  Out of These World
//
//  Created by Rohit Singh on 04/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     NSString *imageName=[NSString stringWithFormat:@"%@.jpg",self.otobject.name];
    
    self.image=[[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    self.scrollview.contentSize=self.image.frame.size;
    [self.scrollview addSubview:self.image];
    self.scrollview.delegate=self;
    self.scrollview.maximumZoomScale=2.0;
    self.scrollview.minimumZoomScale=0.5;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.image;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
