//
//  OTWObject.h
//  Out of These World
//
//  Created by Rohit Singh on 04/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTWObject : NSObject
@property(nonatomic,strong) NSString *name;

@property(nonatomic) float gravitaionalForce;
@property(nonatomic) float diamaeter;
@property(nonatomic) float yaerLength;
@property(nonatomic) float temperature;
@property(nonatomic) float dayLength;
@property(nonatomic) int numberOfMoons;
@property(nonatomic,strong) NSString *nickName;
@property(nonatomic,strong) NSString *interstFact;
@property(nonatomic,strong) UIImage *spaceImage;

-(id)initWithData:(NSDictionary *)Data andImage:(UIImage *)image;
@end
