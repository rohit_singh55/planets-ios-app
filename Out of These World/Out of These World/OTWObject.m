//
//  OTWObject.m
//  Out of These World
//
//  Created by Rohit Singh on 04/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "OTWObject.h"
#import "AstronomicalData.h"

@implementation OTWObject
-(id)init{
    self=[self initWithData:nil andImage:nil];
    return self;
}


-(id)initWithData:(NSDictionary *)Data andImage:(UIImage *)image{
    self=[super init];
    self.name=Data[PLANET_NAME];
    self.gravitaionalForce=[Data[PLANET_GRAVITY] floatValue];
    self.diamaeter=[Data[PLANET_DIAMETER]floatValue];
     self.dayLength=[Data[PLANET_DAY_LENGTH]floatValue];
     self.yaerLength=[Data[PLANET_YEAR_LENGTH]floatValue];
    self.temperature=[Data[PLANET_TEMPERATURE]floatValue];
    self.numberOfMoons=[Data[PLANET_NUMBER_OF_MOONS]intValue];
    self.nickName=Data[PLANET_NICKNAME];
    self.interstFact=Data[PLANET_INTERESTING_FACT];
    self.spaceImage=image;
    return self;
}
@end
