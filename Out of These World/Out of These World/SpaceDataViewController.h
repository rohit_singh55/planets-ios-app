//
//  SpaceDataViewController.h
//  Out of These World
//
//  Created by Rohit Singh on 05/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OTWObject.h"
@interface SpaceDataViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableVIEW;
@property (strong, nonatomic) OTWObject *oobject;
@end
