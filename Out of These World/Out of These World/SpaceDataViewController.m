//
//  SpaceDataViewController.m
//  Out of These World
//
//  Created by Rohit Singh on 05/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "SpaceDataViewController.h"

@interface SpaceDataViewController ()

@end

@implementation SpaceDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor blackColor];
    self.tableVIEW.backgroundColor=[UIColor clearColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier=@"dataCell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor clearColor];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text=@"NickName : ";
            cell.detailTextLabel.text=self.oobject.nickName;
            break;
        case 1:
            cell.textLabel.text=@"Diameter : ";
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%f",self.oobject.diamaeter];
            break;
        case 2:
            cell.textLabel.text=@"Gravitional Force : ";
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%f",self.oobject.gravitaionalForce];
            break;
        case 3:
            cell.textLabel.text=@"Length OF Year : ";
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%f",self.oobject.yaerLength];
            break;
            case 4:
            cell.textLabel.text=@"Length OF Days : ";
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%f",self.oobject.dayLength];
            break;
        case 5:
            cell.textLabel.text=@"Temperature OF Days : ";
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%f",self.oobject.temperature];
            break;
        case 6:
            cell.textLabel.text=@"Number Of Moons : ";
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%i",self.oobject.numberOfMoons];
            break;
        case 7:
            cell.textLabel.text=@"Interesting Fact : ";
            cell.detailTextLabel.text=self.oobject.interstFact;
            break;
        default:
            break;
    }
    return cell;
    
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}
-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    NSLog(@" working ");
    
}

@end
